package com.twuc.bagSaving;

import java.util.Iterator;
import java.util.Map;

public class StupidAssistant {

    private final Map cabinets;

    public StupidAssistant(Map cabinets) {
        this.cabinets = cabinets;
    }

    public Ticket saveBag(Bag bag, LockerSize lockerSize) {
        Iterator<Map.Entry<Integer, Cabinet>> entries = cabinets.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<Integer, Cabinet> entry = entries.next();
            Cabinet cabinet = entry.getValue();
            try {
                return cabinet.save(bag, lockerSize);
            } catch (Exception e) {
                continue;
            }
        }
        throw new IllegalArgumentException("Invalid capacity: 0");
    }

    public Bag getBag(Ticket ticket) {
        for (Map.Entry<Integer, Cabinet> entry : (Iterable<Map.Entry<Integer, Cabinet>>) cabinets.entrySet()) {
            Cabinet cabinet = entry.getValue();
            try {
                return cabinet.getBag(ticket);
            } catch (Exception e) {
                continue;
            }
        }
        throw new IllegalArgumentException("Invalid capacity: 0");
    }
}

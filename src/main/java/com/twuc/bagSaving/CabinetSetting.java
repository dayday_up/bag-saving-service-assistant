package com.twuc.bagSaving;

public class CabinetSetting {
    private final Integer cabinetNumber;
    private final LockerSetting lockerSetting;

    public CabinetSetting(Integer cabinetNumber, LockerSetting lockerSetting) {
        this.cabinetNumber = cabinetNumber;
        this.lockerSetting = lockerSetting;
    }

    public static CabinetSetting of(Integer cabinetNumber, LockerSetting lockerSetting) {
        return new CabinetSetting(cabinetNumber, lockerSetting);
    }

    public Integer getCabinetNumber() {
        return cabinetNumber;
    }

    public LockerSetting getLockerSetting() {
        return lockerSetting;
    }
}

package com.twuc.bagSaving;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class CabinetService {
    private final Map<Integer, Cabinet> cabinets;
    private StupidAssistant stupidAssistant;

    public CabinetService(Map<Integer, Cabinet> cabinets) {
        this.cabinets = cabinets;
        this.stupidAssistant = new StupidAssistant(cabinets);
    }

    public Ticket saveBag(Bag bag, LockerSize lockerSize) {
        return stupidAssistant.saveBag(bag, lockerSize);
    }

    public Bag getBag(Ticket ticket) {
        return stupidAssistant.getBag(ticket);
    }
}
